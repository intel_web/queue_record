<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\City */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="city-form">

    <?php $form = ActiveForm::begin(); ?>

        <div class="row">
            <div class="col-md-4">
                
                <?= $form->field($filterModel, 'fromDate')->widget(DatePicker::classname(), [
                    'options' => ['placeholder' => 'Выберите дату...'],
                    'removeButton' => [
                        'icon'=>'trash',
                    ],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                    ]
                ]) ?>

            </div>
            <div class="col-md-4">
                
                <?= $form->field($filterModel, 'toDate')->widget(DatePicker::classname(), [
                    'options' => ['placeholder' => 'Выберите дату...'],
                    'removeButton' => [
                        'icon'=>'trash',
                    ],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                    ]
                ]) ?>

            </div>
            <div class="col-md-4 text-right" style="margin:20px 0 0 0">

                <?= Html::submitButton('Показать статистику', ['class' => 'btn btn-success']) ?>

            </div>
        </div>

    <?php ActiveForm::end(); ?>

</div>
