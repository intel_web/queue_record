<?php

namespace common\models;

use Yii;
use yii\helpers\Html;

/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property string $citizen_name
 * @property string $city
 * @property string $address
 * @property string $phone
 * @property string $appeal
 * @property integer $earlier_appeal_id
 * @property string $earlier_appeal_text
 * @property integer $reception_time_id
 *
 * @property ReceptionTime $receptionTime
 * @property EarlierAppeal $earlierAppeal
 */
class Order extends \yii\db\ActiveRecord
{
    public $count;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules() 
    { 
        return [
            [['citizen_name', 'sex', 'birth_date', 'city', 'street', 'house', 'classifier_id', 'appeal', 'earlier_appeal_id','reception_time_id'], 'required'],
            [['sex', 'classifier_id', 'earlier_appeal_id'], 'integer'],
            [['birth_date', 'age'], 'safe'],
            [['appeal'], 'string'],
            [['citizen_name', 'age', 'city', 'street', 'house', 'apartment_number', 'earlier_appeal_text'], 'string', 'max' => 255],
            ['email', 'email'],
            [['phone'], 'string', 'min' => 5, 'max' => 15, 'skipOnEmpty' => true, 'skipOnError' => false],
            ['phone', 'match', 'pattern'=>'/^[0-9_-]+$/', 'message' => 'Вы можете ввести только цифры и знак " - "'],
            ['reception_time_id', 'integer','message' => 'Время не выбрано'],
            
            [['phone'], 'required', 'when' => function ($model) {
                return $model->email == '';
                }, 
                'whenClient' => "function (attribute, value) {
                    if($('#order-phone').val() == '' && $('#order-email').val() == '')
                    {
                        $('.field-order-phone').addClass( 'has-error' ).find('.help-block-error').text( 'Необходимо заполнить хотя бы одно из полей «Телефон» или «Эл.Почта»');
                    return true;
                } else { 
                    $('.field-order-phone').removeClass( 'has-error' ).find('.help-block-error').text('');
                    return false;
                }}", 'message' => 'Необходимо заполнить хотя бы одно из полей «Телефон» или «Эл.Почта»'],
        ]; 
    }

    /**
     * @inheritdoc
     */

    public function attributeLabels() 
    { 
        return [ 
            'id' => 'ID',
            'citizen_name' => 'ФИО',
            'sex' => 'Пол',
            'birth_date' => 'Дата рождения',
            'age' => 'Возраст',
            'city' => 'Город, сельское поселение, село',
            'street' => 'Улица',
            'house' => 'Дом',
            'apartment_number' => 'Квартира',
            'phone' => 'Контактный телефон',
            'email' => 'Email',
            'classifier_id' => 'Категория обращения',
            'appeal' => 'Текст обращения',
            'earlier_appeal_id' => 'Ранние обращения',
            'earlier_appeal_text' => 'Ранние обращения',
            'reception_time_id' => 'Время приёма',
        ]; 
    } 

    /** 
     * @return \yii\db\ActiveQuery 
     */ 
    public function getClassifier() 
    { 
        return $this->hasOne(Classifier::className(), ['id' => 'classifier_id']);
    } 
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReceptionTime()
    {
        return $this->hasOne(ReceptionTime::className(), ['id' => 'reception_time_id']);
    }

    public function getReceptionDay()
    {
        return $this->hasOne(ReceptionDay::className(), ['id' => 'reception_day_id'])
            ->via('receptionTime');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEarlierAppeal()
    {
        return $this->hasOne(EarlierAppeal::className(), ['id' => 'earlier_appeal_id']);
    }

    /*
        this method returns string with instance name of early appeals
    */
    public function getEarlierAppealByOrderId($id)
    {
        if(empty($this->earlier_appeal_text))
        {
            return EarlierAppeal::findOne($this->earlier_appeal_id)->instance;            
        } else {
            return $this->earlier_appeal_text;
        }
    }

    /*
        Метод использовался на order/view,
        заменен на substr($model->receptionTime->rec_time,0,-3)
        на самой view у поля время приема. Временно не используется.
    */

    public function getReceptionTimeToString($reception_time_id)
    {
        $receptionTime = ReceptionTime::findOne($reception_time_id)->rec_time;
        if(!empty($receptionTime))
        {
            $receptionTime = substr($receptionTime,0,-3);
        } else {
            $receptionTime = 'Время не назначено';
        }
        return $receptionTime;
    }

    /*
        Метод возвращает ближащую дату приема
    */
    public function getNearestDate()
    {
        $nearest_date = ReceptionDay::find()
            ->where('reception_date > :current_date AND status = 1', [':current_date' => date('Y-m-d')])
            ->with(['receptionTimes' => function($query) {
                $query->andWhere(['status' => 0]);
            }])
            ->orderBy('reception_date')
            ->one();
        if (count($nearest_date['receptionTimes'])) {
            return $nearest_date;
        } else {
            return null;
        }
    }

    public function isFreeDayExists() {
        $nearest_date = ReceptionDay::find()
            ->where('reception_date > :current_date', [':current_date' => date('Y-m-d')])
            ->andWhere('status = :published', [':published' => 1])
            ->andWhere(['exists', ReceptionTime::find()
                ->where('status = :free AND reception_day_id = reception_day.id', [':free' => 0])
            ])
            ->exists();
            
        return $nearest_date;
    }

    public function getJSONNearestDate() {
        $nearest_date = ReceptionDay::find()
            ->where('reception_date > :current_date AND status = 1', [':current_date' => date('Y-m-d')])
            ->with(['receptionTimes'])
            ->orderBy('reception_date')
            ->asArray()
            ->one();
        return $nearest_date;
    }

    public function getJSONReceptionDays() {
        $receptionDays = ReceptionDay::find()
            ->where('reception_date > :current_date AND status = 1', [':current_date' => date('Y-m-d')])
            ->with(['receptionTimes'])
            ->orderBy('reception_date')
            ->asArray()
            ->all();
        return $receptionDays;
    }

    public function getOrdersCountInMonth() {
        $nearest_date = ReceptionDay::find()
            ->where('reception_date >= :current_date AND status = 1', [':current_date' => date('Y-m-d')])
            ->with(['receptionTimes' => function($query) {
                $query->andWhere(['status' => 1]);
            }])
            ->orderBy('reception_date')
            ->one();
        return count($nearest_date['receptionTimes']);
    }

    public function getNearestDateToString()
    {
        $model = $this->getNearestDate();
        if(is_object($model) and $model->status === 1)
        {
            return $model->reception_date;
        } else {
            return Html::encode('дата не установлена');
        }
    }

    /** Метод возвращает адрес сформировав его из полей 
    * city, street, house и apartment_number,
    * если оно не пусто
    */
    public function getFullAddress($id)
    {
        $fullAddress = "";
        if (($model = Order::findOne($id)) !== null) {
            $fullAddress .= $model->city.', '.$model->street.' '.$model->house;
            if($model->apartment_number!=null) {
                $fullAddress .= ', кв.'.$model->apartment_number;
            }
        }
        return $fullAddress;
    } 
    /*
        Метод должен возвращать колличество записей на прием на ближайщий день приема
    */
    public function getCurrentOrderNumber()
    {
        $count = 0;
        $model=$this->getNearestDate();
        if(is_object($model) and $model->status === 1)
        {
            $count = ReceptionTime::find()->where('reception_day_id = :reception_day_id AND status = 1',[':reception_day_id' => $model->id])->count();
        } 
        return $count;
    }

    public static function getCityStatByRange($from, $to) {
        return Order::find()
            ->select(['city, COUNT(*) AS count'])
            ->joinWith('receptionDay')
            ->where("reception_day.reception_date BETWEEN :from AND :to", [':from' => $from, ':to' => $to])
            ->groupBy(['city'])
            ->asArray()
            ->all();
    }

    public static function getSexStatByRange($from, $to) {
        return Order::find()
            ->select(['sex, COUNT(*) AS count'])
            ->joinWith('receptionDay')
            ->where("reception_day.reception_date BETWEEN :from AND :to", [':from' => $from, ':to' => $to])
            ->groupBy(['sex'])
            ->asArray()
            ->all();
    }
    
    public static function getAgeStatByRange($from, $to) {
        return Order::find()
            ->select(['age, COUNT(*) AS count'])
            ->joinWith('receptionDay')
            ->where("reception_day.reception_date BETWEEN :from AND :to", [':from' => $from, ':to' => $to])
            ->groupBy(['age'])
            ->asArray()
            ->all();
    } 

    public static function getClassifierStatByRange($from, $to) {
        return Order::find()
            ->select(['classifier.classifier_name, COUNT(*) AS count'])
            ->joinWith('receptionDay')
            ->joinWith('classifier')
            ->where("reception_day.reception_date BETWEEN :from AND :to", [':from' => $from, ':to' => $to])
            ->groupBy(['classifier.classifier_name'])
            ->asArray()
            ->all();
    } 

    /** Метод проверяет записался ли гражданин на прием в выбранный день. 
    *    Необходим для исключения дубликатов. 
    *   Вынесен в отдельный метод, чтобы вызывать только в actionCreate, 
    *   т.к. beforeSave вызывается так же в actionUpdate. 
    */

    public function isOrderExists($model)
    {
        if (Order::find()->joinWith(['receptionDay'])
                ->where('order.citizen_name like :citizen_name AND order.birth_date = :birth_date AND order.phone = :phone 
                    AND reception_day.id = :reception_day_id',
                [':citizen_name' => $model->citizen_name,
                ':birth_date' => $model->birth_date,
                ':phone' => $model->phone,
                ':reception_day_id' => $model->receptionTime->receptionDay->id])->exists() ) {
                
            return true;
        } else {
            return false;
        } 
    }
    
    public function beforeSave($insert)
    {
        if(parent::beforeSave($insert)) 
        {
            $birthday_timestamp = strtotime($this->birth_date);
            $age = date('Y') - date('Y', $birthday_timestamp);
            if (date('md', $birthday_timestamp) > date('md')) {
                $age--;
            }
            $this->age = (string) $age;
            return true;
        }        
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $receptionTime = ReceptionTime::find()->where('id = :reception_time_id', 
                    [':reception_time_id' => $this->reception_time_id])->one();
        $receptionTime->status = '1';
        $receptionTime->save();
    }

    public function afterDelete()
    {
        parent::afterDelete();

        $receptionTime = ReceptionTime::find()->where('id = :reception_time_id', 
                    [':reception_time_id' => $this->reception_time_id])->one();
        $receptionTime->status = '0';
        $receptionTime->save();
    }
}
