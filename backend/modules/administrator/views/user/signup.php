<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\AuthItem;
use yii\helpers\ArrayHelper;


$this->title = 'Регистрация нового пользователя';
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['user/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="panel panel-success block-shadow">
        <div class="panel-heading text-center">
            <h1>Данные для входа пользователя</h1>  
        </div>

        <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
        
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4">

                        <?= $form->field($model, 'username') ?>

                    </div>
                    <div class="col-md-4">
                        
                        <?= $form->field($model, 'role_name')->dropDownList(ArrayHelper::map(AuthItem::find()->andWhere('type=1')->all(), 'name', 'description')) ?>

                    </div>
                    <div class="col-md-4">
                        
                        <?= $form->field($model, 'email') ?>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        
                        <?= $form->field($model, 'password')->passwordInput(['maxLength' => true]) ?>

                    </div>
                    <div class="col-md-6">
                        
                        <?= $form->field($model, 'confirmPassword')->passwordInput(['maxLength' => true]) ?>

                    </div>
                </div>
                <div class="form-group">
                    <?= Html::submitButton('Добавить', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                </div>
            </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
