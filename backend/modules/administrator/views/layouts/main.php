<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AdminAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
//use yii\widgets\Alert;


AdminAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>

<div class="wrap">
    <?php
	
    NavBar::begin([
        'brandLabel' => 'Панель администратора',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-default navbar-fixed-top',
        ],
    ]);
    $menuItems[] = ['label' => 'Дни приёма', 'url' => ['date/index']];
    $menuItems[] = ['label' => 'Заявки', 'url' => ['order/index']];
    $menuItems[] = ['label' => 'Справочники', 'url' => ['index'],
        'items' => [
            ['label' => 'Инстанции', 'url' => ['earlier-appeal/index']],
            ['label' => 'Классификатор', 'url' => ['classifier/index']],
        ]
    ];
    $menuItems[] = ['label' => 'Статистика', 'url' => ['stat/index']];
    $menuItems[] = ['label' => 'Пользователи', 'url' => ['user/index']];
	
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Вход', 'url' => ['/site/login']];
    } else {
        $menuItems[] = [
            'label' => 'Выход ( '.Yii::$app->user->identity->username.' )',
            'url' => ['/site/logout'],
            'linkOptions' => ['data-method' => 'post']
        ];
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Система записи на прием к Главе администрации <?= date('Y') ?></p>

        <p class="pull-right">
            Разработано <a href="http://its-spc.ru">ИнтелТранс</a>
        </p>
    </div>
</footer>

<?php $this->endBody() ?>

</body>
</html>
<?php $this->endPage() ?>
