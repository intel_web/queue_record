<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

?>

<div class="container">

        <div class="alert alert-<?= ($type) ? ($type) : ('undefined') ?>">
            <?= nl2br(Html::encode($message)) ?>
        </div>

</div>
