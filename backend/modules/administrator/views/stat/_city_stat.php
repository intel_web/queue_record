<?php

use miloschuman\highcharts\Highcharts;

/* @var $this yii\web\View */
/* @var $model common\models\City */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="city-stat">
	 <?=
        Highcharts::widget([
            'scripts' => [
                'modules/exporting',
                'themes/grid-light',
            ],
            'options' => [
                'title' => [
                    'text' => 'Города',
                ],
                'xAxis' => [
                    'categories' => ['Города'],
                ],
                'labels' => [
                ],
                'series' => $cityData->series,
            ]
        ]);
    ?>
</div>
