<?php

namespace app\modules\administrator\controllers;

use Yii;
use common\models\EarlierAppeal;
use common\models\EarlierAppealSearch;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;

use app\modules\administrator\controllers\DefaultController;

class EarlierAppealController extends DefaultController {

    public function actionIndex() {    	
        $searchModel = new EarlierAppealSearch();
    	$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    	return $this->render('index', [
    		'searchModel' => $searchModel,
    		'dataProvider' => $dataProvider,
    	]);
    }
    
    public function actionCreate() {
        $model = new EarlierAppeal();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            if ($model->hasErrors()) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $this->getModelErrorsToString($model->errors);
            } else {
                return $this->renderAjax('_earlier-appeal-form', [
                    'model' => $model,
                ]);                
            }
        }
    }

    public function actionUpdate($id) {
        $model = EarlierAppeal::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            if ($model->hasErrors()) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $this->getModelErrorsToString($model->errors);
            } else {
                return $this->renderAjax('_earlier-appeal-form', [
                    'model' => $model,
                ]);                
            }
        }
    }

    public function actionDelete($id) {
    	if (!$this->findModel($id)->delete()) {
    		Yii::$app->getSession()->setFlash('error', 'Невозможно удалить эту запись, т.к. она связана с таблицей заявок');
    	} else {
    		Yii::$app->getSession()->setFlash('success', 'Запись удалена');
    	}

    	return $this->redirect(['index']);
    }

    protected function getModelErrorsToString($errors) {
        $errorString = "";
        foreach ($errors as $error) {
            for ($i = 0, $maxi = count($error); $i < $maxi; $i++) {
                $errorString .= $error[$i]."\r\n";
            }
        }
        return $errorString;
    }

    protected function findModel($id) {
    	if (($model = EarlierAppeal::findOne($id)) !== null) {
    		return $model;
    	} else {
    		throw new NotFoundException('Данная интсанция отсутствует...');
    	}
    }
    
}
