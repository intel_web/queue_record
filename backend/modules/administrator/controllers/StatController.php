<?php

namespace app\modules\administrator\controllers;

use Yii;
use common\models\City;
use common\models\ReceptionDay;
use common\models\Order;
use app\modules\administrator\models\OrderStatFilterForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\JsExpression;

/**
 * StatController implements the CRUD actions for calculating stat.
 */
class StatController extends DefaultController {
    
    public function actionIndex() {
        $filterModel = new OrderStatFilterForm();
        $cityData = new \stdClass();
        $sexData = new \stdClass();
        $classifierData = new \stdClass();
        $ageData = new \stdClass();
        $orders = array();

        if ($filterModel->load(Yii::$app->request->post())) {
            $cityData = $this->getCityColData(Order::getCityStatByRange($filterModel->fromDate, $filterModel->toDate));
            $sexData = $this->getSexPieData(Order::getSexStatByRange($filterModel->fromDate, $filterModel->toDate));
            $ageData = $this->getAgePieData(Order::getAgeStatByRange($filterModel->fromDate, $filterModel->toDate));
            $classifierData = $this->getClassifierColData(Order::getClassifierStatByRange($filterModel->fromDate, $filterModel->toDate));
        }

        return $this->render('index', [
            'filterModel' => $filterModel,
            'cityData' => $cityData,
            'sexData' => $sexData,
            'ageData' => $ageData,
            'classifierData' => $classifierData,
        ]);
    }

    protected function getClassifierColData($orders) {
        $data = new \stdClass();
        $data->series = array();
        for($i = 0, $maxi = count($orders); $i < $maxi; $i++) {
            array_push($data->series, ['type' => 'column', 'name' => $orders[$i]['classifier_name'], 'data' => [intval($orders[$i]['count'])]]);
        }
        return $data;
    }

    protected function getCityColData($orders) {
        $data = new \stdClass();
        $data->series = array();
        for($i = 0, $maxi = count($orders); $i < $maxi; $i++) {
            array_push($data->series, ['type' => 'column', 'name' => $orders[$i]['city'], 'data' => [intval($orders[$i]['count'])]]);
        }
        return $data;
    }

    protected function getAgePieData($orders) {

        $firstAge=0;
        $secondAge=0;
        $thirdAge=0;
        $fourthAge=0;
        $fifthAge=0;
        $sixthAge=0;

        $data = new \stdClass();
        $data->series = array();

        for($i = 0, $maxi = count($orders); $i < $maxi; $i++) {
            if( (integer)$orders[$i]['age']>0 && (integer)$orders[$i]['age']<20 )
            {
                $firstAge++;
            } elseif ((integer)$orders[$i]['age']>19 && (integer)$orders[$i]['age']<30) 
            {
                $secondAge++;
            } elseif ((integer)$orders[$i]['age']>29 && (integer)$orders[$i]['age']<40) {
                $thirdAge++;
            } elseif ((integer)$orders[$i]['age']>39 && (integer)$orders[$i]['age']<50) {
                $fourthAge++;
            } elseif ((integer)$orders[$i]['age']>49 && (integer)$orders[$i]['age']<60) {
                $fifthAge++;
            } elseif ((integer)$orders[$i]['age']>59) {
                $sixthAge++;
            }
        }

        array_push($data->series, ['name'=>'1-19 лет','y' => $firstAge,'color'=>'#7cb5ec'],
                                ['name'=>'20-29 лет','y' => $secondAge,'color'=>'#f7a35c'],
                                ['name'=>'30-39 лет','y' => $thirdAge,'color'=>'#90ee7e'],
                                ['name'=>'40-49 лет','y' => $fourthAge,'color'=>'#ff0066'],
                                ['name'=>'50-59 лет','y' => $fifthAge,'color'=>'#7798BF'],
                                ['name'=>'Старше 60 лет','y' => $sixthAge,'color'=>'#55BF3B']);
        return $data;
    }    

    protected function getSexPieData($orders) {
        $data = new \stdClass();
        $data->series = array();
        for($i = 0, $maxi = count($orders); $i < $maxi; $i++) {
            array_push($data->series, [
                'name' => (intval($orders[$i]['sex'])) ? ('Мужчин') : ('Женщин'),
                'y' => intval($orders[$i]['count']),
                'color' => new JsExpression('Highcharts.getOptions().colors['.$i.']'),
            ]);
        }
        return $data;
    }
}
