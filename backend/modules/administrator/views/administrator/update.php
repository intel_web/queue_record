<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Administrator */

$this->title = 'Обновить администратора: ' . ' ' . $model->user->username;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['user/index']];
$this->params['breadcrumbs'][] = ['label' => 'Администраторы', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="administrator-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
