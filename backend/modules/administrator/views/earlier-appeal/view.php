<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->instance;
$this->params['breadcrumbs'][] = ['label' => 'Инстанции', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="earlier-appeal-view">

	<?= 

		DetailView::widget([
			'model' => $model,
			'attributes' => [
				'id',
				'instance'
			],
		]);

	?>

</div>
