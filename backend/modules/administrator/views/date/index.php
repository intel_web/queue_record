<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use common\helpers\DateTranslater;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
use yii\bootstrap\Modal;

$this->title = "Дни приёма";
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="date-index">
	<h1>
	
		<?= Html::encode($this->title) ?>

	</h1>
	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-default">
		        <div class="panel-heading text-center">
		        	<h3>Загрузить план приёма Главы</h3>
		        </div>
		        <div class="panel-body">

					<?php $form = ActiveForm::begin([
						'action' => ['date/upload'], 
						'options' => ['enctype' => 'multipart/form-data'],
					]); ?>

						<?=
							$form->field($schedule_file, 'file')->fileInput()->label('Файл расписания')
						?>

						<div class="form-group">
							
							<?= Html::submitButton('Загрузить', ['class' => 'btn btn-success']) ?>

						</div>

					<?php ActiveForm::end(); ?>

		  		</div>
		    </div>	
		</div>
	</div>

	<?php 

		Modal::begin([
			'header' => 'День приёма',
			'id' => 'editModalId',
			'class' => 'modal',
			'size' => 'modal-md',
		]);

	?>
		<div id="time-progressbar" class="progress">
			<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%">
				<span class="sr-only">45% Complete</span>
			</div>
		</div>
		<div class='modalContent'></div>

	<?php Modal::end(); ?>

	<div class="form-group">
			
		<?= Html::a('Добавить день приёма', ['create-date'], ['class'=>'btn btn-success modalButton', 'title'=>'view/edit']) ?>

	</div>

	<?=
		GridView::widget([
			'dataProvider' => $dataProvider,
			'filterModel' => $searchModel,
			'columns' => [
				['class' => 'yii\grid\SerialColumn'],
				[
					'attribute' => 'reception_date',
					'filter' => DatePicker::widget([
						'model' => $searchModel,
						'attribute' => 'reception_date',
						'language' => 'ru',
						'clientOptions' => [
							'autoclose' => true,
							'format' => 'yyyy-mm-dd',
							'clearBtn' => true,
						],
					]),
					'value' => function($model) {
						return DateTranslater::translateDate($model->reception_date);
					},
				],
				[
					'attribute' => 'status',
					'filter' => array('1' => 'Опубликован', '0' => 'На утверждении'),
					'value' => function($model) {
						return ($model->status) ? ('Опубликован') : ('На утверждении');
					},
				],
				[
					'class' => 'yii\grid\ActionColumn',
                	'template' => '{view}{update}{delete}',
	                'buttons' => [
	                	'view' => function ($url, $model) {
	                		return Html::a('<span class="glyphicon glyphicon-eye-open">&nbsp</span>', ['view?id='.$model->id],
	                            ['title'=>'Просмотр']);
	                	},
	                    'update' => function ($url,$model) {
	                        return Html::a('<span class="glyphicon glyphicon-pencil">&nbsp</span>', ['edit-date?id='.$model->id],
	                            ['title'=>'Редактировать', 'class' => 'modalButton']);
	                    },
	                    'delete' => function ($url,$model) {
	                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete?id='.$model->id],
	                            ['title'=>'Удалить',
	                            'data-confirm' => Yii::t('yii', 'Вы действительно хотите удалить время приёма?'),
	                            ]);
	                    }
	                ],
            	],
			],
		]);
	?>

</div>
