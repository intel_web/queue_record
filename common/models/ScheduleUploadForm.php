<?php

namespace common\models;

use yii\base\Model;
use yii\web\UploadedFile;
use Yii;

class ScheduleUploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $file;
    public $fileName;

    public function rules()
    {
        return [
            ['file', 'required'],
            [['file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'doc', 'maxSize' => 4*1024*1024, 'minSize' => 5*1024],
        ];
    }

    public function attributeLabels() {
        return [
            'file' => 'План приёма Главы',
        ];
    }

    public function upload()
    {
        if($this->validate()) {
            $this->file->saveAs(Yii::getAlias('@uploads')."/". $this->fileName . '.' . $this->file->extension);
            return true;
        }else{
            return false;
        }
    }
}

?>