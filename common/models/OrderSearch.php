<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Order;

/**
 * OrderSearch represents the model behind the search form about `common\models\Order`.
 */
class OrderSearch extends Order
{
    public $receptionDate;
    public $recTime;
    /**
     * @inheritdoc
     */
    public function rules() 
    { 
        return [ 
            [['id', 'sex', 'classifier_id', 'earlier_appeal_id', 'reception_time_id'], 'integer'],
            [['citizen_name', 'birth_date', 'age', 'city', 'street', 'house', 'apartment_number', 'phone', 'email', 'appeal', 'earlier_appeal_text','receptionDate'], 'safe'],
        ]; 
    } 

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 15,
            ],
            /*'sort' => [
                'defaultOrder' => [
                    'receptionTime.receptionDay.reception_date' => SORT_ASC, 
                ],
            ],*/
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id',
                'citizen_name',
                'city',
                'address',
                'phone',
                'appeal',
                'earlier_appeal_id',                
                'earlier_appeal_text',
                'reception_time_id',                
                'receptionDate' => [
                    'asc' => ['reception_day.reception_date' => SORT_ASC],
                    'desc' => ['reception_day.reception_date' => SORT_DESC],
                    'label' => 'Data Priema',
                ],
                'recTime' => [
                    'asc' => ['reception_time.rec_time' => SORT_ASC],
                    'desc' => ['reception_time.rec_time' => SORT_DESC],
                    'label' => 'Время приёма',
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            $query->joinWith(['receptionTime.receptionDay']);
            $query->joinWith(['receptionTime']);

            return $dataProvider;
        }

        //$query->orderBy('reception_day.reception_date', 'DESC');

        $query->andFilterWhere([
            'id' => $this->id,
            'sex' => $this->sex,
            'birth_date' => $this->birth_date,
            'classifier_id' => $this->classifier_id,
            'earlier_appeal_id' => $this->earlier_appeal_id,
            'reception_time_id' => $this->reception_time_id,
        ]);

        $query->andFilterWhere(['like', 'citizen_name', $this->citizen_name])
            ->andFilterWhere(['like', 'age', $this->age])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'street', $this->street])
            ->andFilterWhere(['like', 'house', $this->house])
            ->andFilterWhere(['like', 'apartment_number', $this->apartment_number])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'appeal', $this->appeal])
            ->andFilterWhere(['like', 'earlier_appeal_text', $this->earlier_appeal_text]);

        $query->joinWith(['receptionTime.receptionDay' => function ($q) {
            $q->where('reception_day.reception_date LIKE "%' . $this->receptionDate . '%"');
        }]);
        $query->joinWith(['receptionTime' => function ($q) {
            $q->where('reception_time.rec_time LIKE "%' . $this->recTime . '%"');
        }]);

        $query->orderBy('reception_day.reception_date DESC');

        return $dataProvider;
    }
}
