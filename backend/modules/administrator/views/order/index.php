<?php

use yii\helpers\Html;
use yii\grid\GridView;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel common\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заявки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">

    <h3>

        <?= Html::tag('p', Html::encode($this->title), ['class' => 'alert alert-success text-center']) ?>
    
    </h3>
    <p>
    
        <?= Html::a('Добавить заявку', ['create'], ['class' => 'btn btn-success']) ?>
    
    </p>

    <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'citizen_name',
                [
                    'label' => 'Населенный пункт',
                    'attribute' => 'city',
                ],
                'phone',
                [
                    'label' => 'Дата приема',
                    'attribute' => 'receptionDate',
                    'filter' => DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'receptionDate',
                        'language' => 'ru',
                        'clientOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd',
                            'clearBtn' => true,
                        ],
                    ]),
                    'value' => 'receptionTime.receptionDay.reception_date',
                ],
                [
                    'label' => 'Время приёма',
                    'attribute' => 'recTime',
                    'value' => function($model) {
                        return $model->receptionTime->getTime($model->receptionTime->rec_time);
                    },
                ],
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]);
    ?>

</div>
