<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "earlier_appeal".
 *
 * @property integer $id
 * @property string $instance
 *
 * @property Order[] $orders
 */
class EarlierAppeal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'earlier_appeal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['instance'], 'required'],
            [['instance'], 'unique'],
            [['instance'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'instance' => 'Инстанция',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['earlier_appeal_id' => 'id']);
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {
            $counter = Order::find()->where('earlier_appeal_id = :earlier_appeal_id' , [':earlier_appeal_id' => $this->id])->count();

            if ($counter != 0) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }
}
