<?php
return [
    'siteTtitle' => 'Электронная запись граждан на личный прием к Главе администрации',
    'adminEmail' => 'arslan92@mail.ru',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'scheduleFileName' => 'schedule',
];
