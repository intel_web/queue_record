<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "reception_time".
 *
 * @property integer $id
 * @property integer $reception_day_id
 * @property string $rec_time
 * @property integer $status
 *
 * @property Order[] $orders
 * @property ReceptionDay $receptionDay
 */
class ReceptionTime extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reception_time';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['reception_day_id', 'rec_time'], 'required'],
            [['reception_day_id', 'status'], 'integer'],
            [['rec_time'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'reception_day_id' => 'Reception Day ID',
            'rec_time' => 'Время приёма',
            'status' => 'Статус',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['reception_time_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReceptionDay()
    {
        return $this->hasOne(ReceptionDay::className(), ['id' => 'reception_day_id']);
    }

    /*
        This method return time string without seconds,
        input format hh:mm:ss,
        output format hh:mm
    */

    public function getTime($rec_time){
        if(!empty($rec_time)){
            return $rec_time = substr($rec_time,0,-3);
        }else{
            return '';
        }
    }

    public static function getFreeTime($reception_day_id) {
        return ReceptionTime::find()
            ->where('reception_day_id = :reception_day_id AND status = 0', [':reception_day_id' => $reception_day_id])
            ->orderBy('rec_time')
            ->all();
    }

    public static function getAllTime($reception_day_id) {
        return ReceptionTime::find()
            ->where('reception_day_id = :reception_day_id', [':reception_day_id' => $reception_day_id])
            ->orderBy('rec_time')
            ->all();
    }

    public static function getReservedTime($reception_day_id) {
        return ReceptionTime::find()
            ->where('reception_day_id = :reception_day_id AND status = 1', [':reception_day_id' => $reception_day_id])
            ->orderBy('rec_time')
            ->all();
    }

    public static function isReceptionTimeFree($id) {
        return ReceptionTime::find()
            ->where('id = :reception_time_id AND status = 1', [':reception_time_id' => $id])
            ->exists();
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                if (ReceptionTime::find()
                    ->where('reception_day_id = :reception_day_id AND rec_time = :rec_time', [
                            ':reception_day_id' => $this->reception_day_id,
                            ':rec_time' => $this->rec_time
                        ])
                    ->exists()) {
                        $this->addError('rec_time', 'Такое время в этот день уже создано.');
                        return false;
                    } else {
                        return true;
                    }
            } else {
                if (ReceptionTime::find()
                    ->where('reception_day_id = :reception_day_id AND rec_time = :rec_time AND id != :id', [
                            ':reception_day_id' => $this->reception_day_id,
                            ':rec_time' => $this->rec_time,
                            ':id' => $this->id
                        ])
                    ->exists()) {
                        $this->addError('rec_time', 'Такое время в этот день уже создано.');
                        return false;
                    } else {
                        return true;
                    }
            }
        } else {
            return false;
        }
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {
           if(Order::find()->where(['reception_time_id'=>$this->id])->one()!=null){
            return false;
           }else{
            return true;
           }
        } else {
            return false;
        }
    }
}
