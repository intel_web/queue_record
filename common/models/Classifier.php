<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "classifier".
 *
 * @property integer $id
 * @property string $classifier_name
 * @property integer $counter
 *
 * @property Order[] $orders
 */
class Classifier extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'classifier';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['classifier_name'], 'required'],
            [['classifier_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'classifier_name' => 'Название категории',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['classifier_id' => 'id']);
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {
           if(Order::find()->where(['classifier_id'=>$this->id])->one()!=null){
            return false;
           }else{
            return true;
           }
        } else {
            return false;
        }
    }
}
