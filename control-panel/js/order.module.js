angular.module('orderApp', ['initialValue']);
angular.module('orderApp').controller('OrderController', function($scope, $http) {
	$scope.receptionDate = '';
	$scope.receptionDayTime = [];
	$scope.selectedDayTime = {};
	$scope.currentReceptionTime = {};
	$scope.order = {};
	$scope.alerts = {};

	$scope.$watch('receptionDate', function(newVal, oldVal) {
		if (newVal !== oldVal) {
			$scope.getReceptionTime($scope.receptionDate, $scope.order.reception_time_id);
		}
	});
	$scope.getReceptionTime = function(receptionDate, reception_time_id) {
		$scope.clearAlerts();
		$('#progress-bar').show();
		$('#progress-bar .progress-bar').css('width', '50%');
		$http.post('get-reception-time', {
			data: {
				receptionDate: receptionDate,
				reception_time_id: reception_time_id,
			},
			_csrf: yii.getCsrfToken(),
			headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
		}).success(function(receptionDayTime) {
			if (receptionDayTime.length) {
				$scope.addAlert('Время приёма загружено', 'alert-success');
			} else {
				$scope.addAlert('Для выбранной даты времена не созданы', 'alert-danger');
			}
			$scope.receptionDayTime = $scope.prepareReceptionDayTime(receptionDayTime);
			$scope.currentReceptionTime = $scope.order.receptionTime;
		}).error(function(error) {
			$scope.addAlert('Не удалось загрузить время приёма', 'alert-danger');
		}).finally(function() {
			$('#progress-bar .progress-bar').css('width', '100%');
			$('#progress-bar').hide();
		});
	};
	$scope.getOrder = function(id) {
		$http.post('get-order', {
			data: {
				orderId: id,
			},
			_csrf: yii.getCsrfToken(),
			headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
		}).success(function(responce) {
			$scope.order = responce;
			$scope.currentReceptionTime = $scope.order.receptionTime;
			$scope.getReceptionTime($scope.receptionDate, $scope.order.reception_time_id);
		}).error(function(error) {
			alert("Ошибка сервера");
		}).finally(function() {
		});
	};
	$scope.prepareReceptionDayTime = function(receptionTime) {
		for (var i = 0, maxi = receptionTime.length; i < maxi; i++) {
			receptionTime[i].rec_time = receptionTime[i].rec_time.split(':')[0] + ':' + receptionTime[i].rec_time.split(':')[1] + "";
		}
		return receptionTime;
	};
	$scope.addAlert = function(message, type) {
		$scope.alerts[type] = $scope.alerts[type]||[];
		$scope.alerts[type].push(message);
	};

	$scope.clearAlerts = function() {
		for(var x in $scope.alerts) {
           delete $scope.alerts[x];
        }
	};

	//init model
	angular.element(document).ready(function () {
		//get model
		$scope.getOrder($scope.order.id);
	});
});

//bootstraping module
//angular.bootstrap(document.getElementById("studentOrderApp"), ['studentOrderApp']);