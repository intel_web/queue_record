<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = "Обновить инстанцию";
$this->params['breadcrumbs'][] = ['label' => 'Инстанции', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div id="earlier-appeal-update">
	<h1> <?= Html::encode($this->title) ?> </h1>

	<?=

		$this->render('_form', [
			'model' => $model
		]);

	?>
	
</div>
