<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\EarlierAppeal;

/**
 * EarlierAppealSearch represents the model behind the search form about `common\models\EarlierAppeal`.
 */
class EarlierAppealSearch extends EarlierAppeal {

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['instance'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EarlierAppeal::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'instance', $this->instance]);
        $query->andFilterWhere(['!=', 'instance', 'Другая инстанция']);

        return $dataProvider;
    }
}
