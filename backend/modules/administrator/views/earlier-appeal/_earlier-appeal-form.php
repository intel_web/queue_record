<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<div id="reception-time-form">

	<?php
		$form = ActiveForm::begin([
			'id' => 'modal-form'
		]);
	?>

		<div class="alert alert-danger" role="alert" style="display: none"></div>

		<?= $form->field($model, 'instance')->textInput(['maxlength' => true])->label('Название инстанции') ?>

		<div class="form-group">
			
			<?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Редактировать', ['class' => $model->isNewRecord ? 'btn btn-success':'btn btn-primary']) ?>

		</div>

	<?php ActiveForm::end(); ?>

</div>