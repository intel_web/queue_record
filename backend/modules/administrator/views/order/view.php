<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Order */

$this->title = $model->citizen_name;
$this->params['breadcrumbs'][] = ['label' => 'Заявки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-view">

    <h1>

        <?= Html::encode($this->title) ?>

    </h1>

    <p>
        
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        
        <?=
            Html::a('Удалить', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Вы действительно хотите удалить эту запись?',
                    'method' => 'post',
                ],
            ])
        ?>

    </p>

    <?= 
        DetailView::widget([
            'model' => $model,
            'attributes' => [
                'citizen_name',
                [
                    'label' => 'Пол',
                    'value' => $model->sex ? 'Мужской' : 'Женский',
                ],
                'birth_date',
                'age',
                [
                    'label' => 'Адрес',
                    'value' => $model->getFullAddress($model->id),
                ],
                'phone',
                'email',
                [
                    'label' => 'Категория обращения',
                    'format'=>'raw',
                    'value' => $model->classifier->classifier_name,
                ],
                [
                    'label' => 'Ранние обращения',
                    'format'=>'raw',
                    'value' => $model->getEarlierAppealByOrderId($model->id),
                ],
                [
                    'label' => 'Дата приема',
                    'format'=>'raw',
                    'value' => $model->receptionTime->receptionDay->reception_date,
                ],
                [
                    'label' => 'Время приема',
                    'format'=>'raw',
                    'value' => substr($model->receptionTime->rec_time,0,-3),
                ],
                'appeal:ntext',
            ],
        ])
    ?>

</div>
