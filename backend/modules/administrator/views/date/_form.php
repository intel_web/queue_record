<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
?>
<div id="earlier-appeal-form">

	<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

		<?= 
			$form->field($model, 'reception_date')->widget(
				DatePicker::ClassName(), [
					'language' => 'ru',
					'clientOptions' => [
						'autoclose' => true,
						'format' => 'yyyy-mm-dd',
					]
				])
		?>

		<?= $form->field($model, 'status')->checkbox() ?>

		<div class="form-group">
			
			<?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

		</div>

	<?php ActiveForm::end(); ?>

</div>
