<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "reception_day".
 *
 * @property integer $id
 * @property string $reception_date
 * @property integer $status
 *
 * @property ReceptionTime[] $receptionTimes
 */
class ReceptionDay extends \yii\db\ActiveRecord
{
    public $cnt;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reception_day';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['reception_date'], 'required'],
            [['reception_date'], 'unique'],
            [['status'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'reception_date' => 'День приёма',
            'status' => 'Статус',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReceptionTimes()
    {
        return $this->hasMany(ReceptionTime::className(), ['reception_day_id' => 'id']);
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {
           if(ReceptionTime::find()->where(['reception_day_id'=>$this->id])->one()!=null){
            Yii::$app->getSession()->setFlash('error', 'Невозможно удалить запись');
            return false;
           }else{
            return true;
           }
        } else {
            return false;
        }
    }
}
