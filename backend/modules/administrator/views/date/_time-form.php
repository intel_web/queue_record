<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\time\TimePicker;
?>

<div id="reception-time-form">

	<?php
		$form = ActiveForm::begin([
			'id' => 'modal-form'//$model->formName()
		]);
	?>

		<div class="alert alert-danger" role="alert" style="display: none"></div>

		<?= 
			$form->field($model, 'rec_time')->widget(
				TimePicker::ClassName(), [
					'name' => 'rec_time', 
					'pluginOptions' => [
						'showSeconds' => false,
						'showMeridian' => false,
					]
				])
		?>

		<?=
			$form->field($model, 'status')->dropDownlist(['1' => 'Занято', '0' => 'Свободно'])
		?>

		<div class="form-group">
			
			<?= Html::submitButton($model->isNewRecord ? 'Добавить время' : 'Редактировать время', ['class' => $model->isNewRecord ? 'btn btn-success':'btn btn-primary']) ?>

		</div>

	<?php ActiveForm::end(); ?>

</div>
