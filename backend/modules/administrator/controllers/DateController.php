<?php

namespace app\modules\administrator\controllers;

use Yii;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;
use common\models\ReceptionDay;
use common\models\ScheduleUploadForm;
use common\models\ReceptionTime;
use common\models\ReceptionDaySearch;
use common\models\ReceptionTimeSearch;
use app\modules\administrator\controllers\DefaultController;

class DateController extends DefaultController {
   
    public function actionIndex() {
        $searchModel = new ReceptionDaySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $schedule_file = new ScheduleUploadForm();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'schedule_file' => $schedule_file,
        ]);
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model
            ]);
        }
    }

    public function actionDelete($id) {
        if (!$this->findModel($id)->delete()) {
            Yii::$app->getSession()->setFlash('error', 'Невозможно удалить эту запись, т.к. она связана с таблицей времени приёма');
        } else {
            Yii::$app->getSession()->setFlash('success', 'Запись удалена');
        }

        return $this->redirect(['index']);
    }

    public function actionCreate() {

        $model = new ReceptionDay();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionCreateDate() {
        $model = new ReceptionDay();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            if ($model->hasErrors()) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $this->getModelErrorsToString($model->errors);
            } else {
                return $this->renderAjax('_date-form', [
                    'model' => $model,
                ]);                
            }
        }
    }

    public function actionEditDate($id) {
        $model = ReceptionDay::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            if ($model->hasErrors()) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $this->getModelErrorsToString($model->errors);
            } else {
                return $this->renderAjax('_date-form', [
                    'model' => $model,
                ]);                
            }
        }
    }

    public function actionUpload() {
        $schedule_file = new ScheduleUploadForm();

        $schedule_file->fileName = Yii::$app->params['scheduleFileName'];
        $schedule_file->file = UploadedFile::getInstance($schedule_file,'file');
        if($schedule_file->upload()) {
            Yii::$app->getSession()->setFlash('success', 'План приёма Главы загружен на сервер.');
        } else {
            Yii::$app->getSession()->setFlash('error', 'Произошла ошибка при загрузке файла.');
        }
        return $this->redirect(['index']);
    }

     public function actionView($id) {

        $searchModel = new ReceptionTimeSearch();
        $searchModel->reception_day_id = $id;

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $receptionTime = new ReceptionTime();

        return $this->render('view', [
            'model' => $this->findModel($id),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'reseptionTime' => $receptionTime,
        ]);
    }

    public function actionCreateTime($id) {
        $model = new ReceptionTime();

        $model->reception_day_id = $id;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->reception_day_id]);
        } else {
            if ($model->hasErrors()) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $this->getModelErrorsToString($model->errors);
            } else {
                return $this->renderAjax('_time-form', [
                    'model' => $model,
                ]);                
            }
        }
    }

    public function actionEditTime($id) {
        $model = $this->findReceptionTimeById($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->reception_day_id]);
        } else {
            if ($model->hasErrors()) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $this->getModelErrorsToString($model->errors);
            } else {
                return $this->renderAjax('_time-form', [
                    'model' => $model,
                ]);                
            }
        }
    }

    public function actionDeleteTime($id){

        $model = $this->findReceptionTimeById($id);

        if (!$this->findReceptionTimeById($id)->delete()) {
            Yii::$app->getSession()->setFlash('error', 'Невозможно удалить эту запись, т.к. она связана с таблицей заявок');
        } else {
            Yii::$app->getSession()->setFlash('success', 'Запись удалена');
        }

        return $this->redirect(['view', 'id' => $model->reception_day_id]);

    }

    protected function getModelErrorsToString($errors) {
        $errorString = "";
        foreach ($errors as $error) {
            for ($i = 0, $maxi = count($error); $i < $maxi; $i++) {
                $errorString .= $error[$i]."\r\n";
            }
        }
        return $errorString;
    }

    protected function findModel($id) {
        if (($model = ReceptionDay::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundException('Такой приёмный день не найден');
        }
    }
    
    protected function findReceptionTimeById($id) {
        if (($model = ReceptionTime::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundException('Приёмное время не найден');
        }
    }

}
