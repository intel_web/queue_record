<?php

namespace app\modules\administrator;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\administrator\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
