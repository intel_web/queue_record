<?php

namespace app\modules\administrator\models;

use Yii;

/**
 * This is the model class for table "administrator".
 *
 * @property integer $user_id
 * @property string $surname
 * @property string $name
 * @property string $lastname
 * @property string $phone
 *
 * @property User $user
 */
class Administrator extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'administrator';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['surname', 'name', 'lastname', 'phone'], 'string', 'max' => 255],
            ['surname', 'match', 'pattern' => '/[а-яА-Яч]+$/', 'message' => 'Допустимы буквы русского алфавита.'],
            ['name', 'match', 'pattern' => '/[а-яА-Яч]+$/', 'message' => 'Допустимы буквы русского алфавита.'],
            ['lastname', 'match', 'pattern' => '/[а-яА-Яч]+$/', 'message' => 'Допустимы буквы русского алфавита.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'Имя пользователя',
            'surname' => 'Фамилия',
            'name' => 'Имя',
            'lastname' => 'Отчество',
            'phone' => 'Контактный телефон',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
