<?php
namespace frontend\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use common\models\User;
use common\models\Order;
use common\models\ReceptionTime;
use common\models\ReceptionDay;
use common\helpers\Browser;

use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Controller;
use \yii\web\Response;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                //'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex() {
        $browser = new Browser();
        if ($browser->getBrowser() == Browser::BROWSER_IE && $browser->getVersion() <= 8 ) {
            $this->redirect(['old-browser']);
        }
        $model = new Order();
        if ($model->isFreeDayExists()) {
            $schedule_file = glob(Yii::getAlias('@schedulePath')."/schedule.*");

            return $this->render('index', [
                'model' => $model,
                'schedule_file' => $schedule_file['0'],
            ]);
        } else {
            return $this->render('closed');
        }
    }

    public function actionCreate()
    {
        $model = new Order();

        $model->load(Yii::$app->request->post());
        /*Дополнительная проверка заявки на занятость времени другим пользователем*/
        if(ReceptionTime::find()->where('id = :reception_time_id AND status = 1',
            [':reception_time_id' => $model->reception_time_id])->exists())
        {
            Yii::$app->getSession()->setFlash('error', 'Кажется кто-то опередил Вас, записавшись на выбранное Вами время');
        } else {
            /*Проверка на создание пользователем двух заявок на приём в один день*/
            if($model->isOrderExists($model))
            {
                Yii::$app->getSession()->setFlash('warning', 'Вы уже записались на прием в этот день');
            } else {
                if($model->save())
                {
                    Yii::$app->getSession()->setFlash('success', 'Вы предварительно записаны на прием к Главе района, 
                        сотрудники администрации свяжутся с Вами и сообщат подробности приема');
                } else {
                    Yii::$app->getSession()->setFlash('warning', 'Не удалось сохранить заявку, пожалуйста, попробуйте позднее');
                }
            }
        }

        $this->redirect(['/']);
    }

    public function actionOldBrowser() {
        return $this->render('old-browser');
    }

    public function actionGetNearestReceptionDay() {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $order = new Order();
        return $order->getJSONNearestDate();
    }

    public function actionGetReceptionDays() {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $order = new Order();
        return $order->getJSONReceptionDays();
    }

    //TODO: throw exception if params undefined
    public function actionGetReceptionDayTimes() {
        $params = json_decode(file_get_contents('php://input'));
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($params->data->receptionDayId) {
            $receptionDay = ReceptionDay::findOne($params->data->receptionDayId);
            return $receptionDay->receptionTimes;
        }
    }
}
