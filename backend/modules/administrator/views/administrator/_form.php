<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\User;

/* @var $this yii\web\View */
/* @var $model common\models\Administrator */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="administrator-form">
	<div class="panel panel-success block-shadow">
		<div class="panel-heading text-center">
            <h1>Профиль</h1>  
        </div>

        <?php $form = ActiveForm::begin(); ?>

        	<div class="panel-body">
        		<fieldset>
                    <legend>Общая информация</legend>
                    <div class="row">
                    	<div class="col-md-4">
                    		
                    		<?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>

                    	</div>
                    	<div class="col-md-4">
                    		
                    		<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                    	</div>
                    	<div class="col-md-4">
                    		
                    		<?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>

                    	</div>
                    </div>
                </fieldset>
                <fieldset>
                	<legend>Контактная информация</legend>
                	<div class="row">
                		<div class="col-md-4">
                			
                			<?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

                		</div>
                	</div>
                </fieldset>
			    <div class="form-group">
			        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
			    </div>
        	</div>

    	<?php ActiveForm::end(); ?>

	</div>
</div>
