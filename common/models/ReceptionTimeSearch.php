<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ReceptionTime;

/**
 * ReceptionTimeSearch represents the model behind the search form about `common\models\ReceptionTime`.
 */
class ReceptionTimeSearch extends ReceptionTime
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'reception_day_id', 'status'], 'integer'],
            [['rec_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    *
    * Creates data provider instance with search query applied
    * @param array $params
    * @param int date_id
    * @return ActiveDataProvider
    */
    public function searchByDate($params, $reception_day_id) {
        $query = ReceptionTime::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'reception_day_id' => $reception_day_id,
            'rec_time' => $this->rec_time,
            'status' => $this->status,
        ]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ReceptionTime::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->orderBy('rec_time');

        $query->andFilterWhere([
            'id' => $this->id,
            'reception_day_id' => $this->reception_day_id,
            'rec_time' => $this->rec_time,
            'status' => $this->status,
        ]);

        return $dataProvider;
    }
}
