<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<div id="classifier-form">

	<?php
		$form = ActiveForm::begin([
			'id' => 'modal-form'
		]);
	?>

		<div class="alert alert-danger" role="alert" style="display: none"></div>

		<?= $form->field($model, 'classifier_name')->textInput(['maxlength' => true])->label('Название категории') ?>

		<div class="form-group">
			
			<?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Редактировать', ['class' => $model->isNewRecord ? 'btn btn-success':'btn btn-primary']) ?>

		</div>

	<?php ActiveForm::end(); ?>

</div>