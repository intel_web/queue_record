<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\EarlierAppeal;
use common\models\ReceptionTime;
use common\models\Classifier;
use dosamigos\datepicker\DatePicker;

$this->title = \Yii::$app->params['siteTtitle'];
?>
<div class="row" ng-controller="ErrorController">
    <div class="col-md-12">
        <div ng-repeat="(key,val) in alerts" class="alert {{key}}">
            <div ng-repeat="msg in val">{{msg}}</div>
        </div>
    </div>
</div>
<div class="index-page" ng-controller="DayController">
	<div class="text-center alert alert-success block-shadow">
		<div class="row">
			<div class="col-md-6">
				Выбранный приёмный день: {{receptionDay.reception_date}}
			</div>
			<div class="col-md-6">
				Записались на данный момент – <span class="badge">{{receptionTimeCounter}}</span>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<hr>
				<?php

					if ($schedule_file) echo Html::a('График и время близлежащих приемов – СКАЧАТЬ', [$schedule_file]);

				?>
			</div>
		</div>
	</div>
	<div class="panel panel-success block-shadow" ng-app="orderApp">
		<div class="panel-heading text-center">
			<h1>Запись на прием к Главе администрации</h1>	
		</div>

		<?php $form = ActiveForm::begin(['action' => ['site/create']]); ?>

			<div class="panel-body">
				<fieldset>
					<legend>Общая информация</legend>
					<div class="form-group field-earlierappeal-instance required">

						<?=
							$form->field($model, 'citizen_name')->textInput([
								'maxlength' => true,
								'placeholder'=>"Введите ФИО..."
							])->label('ФИО')
						?>

						<div class="help-block"></div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group field-earlierappeal-instance required">
								
								<?=
									$form->field($model, 'birth_date')->widget(
										DatePicker::ClassName(), [
											'language' => 'ru',
											'clientOptions' => [
												'autoclose' => true,
												'format' => 'yyyy-mm-dd',
											]
									])
								?>
								
								<div class="help-block"></div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group field-earlierappeal-instance required">
								
								<?= 
									$form->field($model, 'sex')->dropDownList(['1'=>'Мужской','0'=>'Женский'],['prompt' => 'Укажите свой пол...'])->label('Пол')
								?>
								
								<div class="help-block"></div>
							</div>
						</div>
					</div>
				</fieldset>
				<fieldset>
					<legend>Адрес</legend>
					<div class="address-form">
						<div class="field" style="display:none">
				            <label>Регион</label>
				            <input type="text" name="region" value="Самарская">
				        </div>
				        <div class="field" style="display:none">
				            <label>Район</label>
				            <input type="text" name="district" value="Кинель-Черкасский">
				        </div>
				        <div class="row">
				        	<div class="col-md-5">
				        		<div class="form-group field-earlierappeal-instance required">

									<?= 
										$form->field($model, 'city')->textInput([
											'maxlength' => true,
											'placeholder' => "Укажите город, сельское поселение...",
											'class' => "form-control",
										])->label('Город, село проживания')
									?>

									<div class="help-block"></div>
								</div>
				        	</div>
				        	<div class="col-md-3">
				        		<div class="form-group field-earlierappeal-instance required">

									<?=
										$form->field($model, 'street')->textInput([
											'maxlength' => true,
											'placeholder' => "Укажите улицу...",
											'class' => "form-control",
											'disabled' => '',
										])->label('Улица')
									?>

									<div class="help-block"></div>
								</div>
				        	</div>
				        	<div class="col-md-2">
				        		<div class="form-group field-earlierappeal-instance required">
									
									<?=
										$form->field($model, 'house')->textInput([
											'maxlength' => true,
											'placeholder' => "Номер дома...",
											'class' => 'form-control',
											'disabled' => '',
										])->label('Дом')
									?>

									<div class="help-block"></div>
								</div>
				        	</div>
				        	<div class="col-md-2">
				        		<div class="form-group field-earlierappeal-instance required">
									
									<?=
										$form->field($model, 'apartment_number')->textInput([
											'maxlength' => true,
											'placeholder' => "Номер квартиры...",
											'class' => 'form-control',
											'disabled' => '',
										])->label('Квартира')
									?>

									<div class="help-block"></div>
								</div>
				        	</div>
				        </div>
					</div>
				</fieldset>
				<fieldset>
					<legend>Контактные данные</legend>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group field-earlierappeal-instance required">
								
								<?=
									$form->field($model, 'phone')->textInput([
										'maxlength' => true,
										'placeholder'=>"Укажите контактный телефон..."
									])->label('Контактный телефон')
								?>

								<div class="help-block"></div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group field-earlierappeal-instance required">
								
								<?= 
									$form->field($model, 'email')->textInput([
										'maxlength' => true,
										'placeholder' => "Укажите адрес электронной почты..."
									])->label('Эл.почта')
								?>

								<div class="help-block"></div>
							</div>
						</div>
					</div>
				</fieldset>

				<fieldset>
					<legend>Содержание обращения</legend>
					<div class="form-group field-earlierappeal-instance required">

						<?=
							$form->field($model, 'appeal')->textarea([
								'rows' => 6,
								'placeholder' => "Текст обращения"
							])->label('Текст обращения')
						?>

						<div class="help-block"></div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group field-earlierappeal-instance required">

								<?=
									$form->field($model, 'classifier_id')->dropDownList(ArrayHelper::map(Classifier::find()->all(), 'id',
					                function($model){
					                    return $model->classifier_name;
					        		}),['prompt' => 'Выберите категорию...'])
					        	?>

								<div class="help-block"></div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group field-earlierappeal-instance required">

								<?=
									$form->field($model, 'earlier_appeal_id')->dropDownList(ArrayHelper::map(EarlierAppeal::find()->orderBy('id DESC')->all(), 'id',
					                function($model){
					                    return $model->instance;
					        		}),['prompt' => 'Выберите инстанцию...', 'id' => 'firstOrganization'])
					        	?>

								<div class="help-block"></div>
							</div>
						</div>
					</div>

					<div class="form-group field-earlierappeal-instance required" id="anotherOrganization" style="display: none;">
						
						<?=
							$form->field($model, 'earlier_appeal_text')->textInput([
								'maxlength' => true,
								'placeholder' => "Укажите инстанцию..."
							])->label('Другая инстанция')
						?>

						<div class="help-block"></div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group field-earlierappeal-instance required">
								<div class="form-group field-order-reception_day">
									<label class="control-label" for="order-reception_time_id">День приёма</label>
									<select id="order-reception_day_id" class="form-control" name="reception_day" ng-options="day as (day.reception_date | dateformat) for day in receptionDays" ng-model="receptionDay">
									</select>
								</div>
								<div class="help-block"></div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group field-earlierappeal-instance required">
								<div class="form-group field-order-reception_time_id required">
									<label class="control-label" for="order-reception_time_id">Время приёма</label>
									<select id="order-reception_time_id" class="form-control" name="Order[reception_time_id]">
										<option value="">Выберите удобное для Вас время приема...</option>
										<option ng-repeat="time in receptionDay.receptionTimes | orderBy:'rec_time'" value="{{time.id}}" ng-disabled="time.status =='1'">{{time.rec_time | timeformat}} {{(time.status == 1) ? ('время занято') : ('')}}</option>
									</select>
								</div>
								<div class="help-block"></div>
							</div>
						</div>
					</div>
				</fieldset>

				<div class="form-group field-earlierappeal-instance required">
					<input type="hidden" name="EarlierAppeal[instance]" value="0">
						<label style="padding-left:20px;">
							<input type="checkbox" id="frontendCheckbox" name="EarlierAppeal[instance]" value="0"> Я подтверждаю согласие на обработку моих персональных данных
						</label>
					<div class="help-block"></div>
				</div>
				<div class="form-group text-center">		
					
					<?=
						Html::submitButton('Записаться на прием', [
							'class' => 'btn btn-success',
							'id' => 'submitButton',
							'disabled' => 'disabled'])
					?>
		        	
		        	<?= Html::a('Отмена', ['index'], ['class' => 'btn btn-success']) ?>
				
				</div>
			</div>

		<?php $form = ActiveForm::end(); ?>

	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="alert alert-warning" role="alert">
				<p>
					Прием граждан проводится в соответствии с утвержденным Административным регламентом рассмотрения обращений граждан.
				</p>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="alert alert-warning" role="alert">
				<p>
					Прием граждан проходит в здании Администрации Кинель-Черкасского района Самарской области по адресу: с.Кинель-Черкассы, ул. Красноармейская, д. 69.Телефон контакта 8-846-604-20-01
				</p>
			</div>
		</div>
	</div>
</div>