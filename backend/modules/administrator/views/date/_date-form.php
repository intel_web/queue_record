<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
?>
<div id="reception-time-form">

	<?php
		$form = ActiveForm::begin([
			'id' => 'modal-form'
		]);
	?>

		<div class="alert alert-danger" role="alert" style="display: none"></div>

		<?= 

			$form->field($model, 'reception_date')->widget(DatePicker::classname(), [
    			'options' => ['placeholder' => 'Выберите дату...'],
    			'removeButton' => [
			        'icon'=>'trash',
			    ],
    			'pluginOptions' => [
        			'autoclose' => true,
        			'format' => 'yyyy-mm-dd',
    			]
			])

		?>

		<?=
			$form->field($model, 'status')->dropDownlist(['0' => 'На утверждении', '1' => 'Опубликован'])
		?>

		<div class="form-group">
			
			<?= Html::submitButton($model->isNewRecord ? 'Добавить приёмный день' : 'Редактировать приёмный день', ['class' => $model->isNewRecord ? 'btn btn-success':'btn btn-primary']) ?>

		</div>

	<?php ActiveForm::end(); ?>

</div>
