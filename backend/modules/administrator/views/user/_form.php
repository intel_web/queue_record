<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">
	<div class="panel panel-success block-shadow">
		<div class="panel-heading text-center">
            <h1>Данные для входа пользователя</h1>  
        </div>

    	<?php $form = ActiveForm::begin(); ?>

    	<div class="panel-body">
	    	<div class="row">
	    		<div class="col-md-6">
	    			
	    			<?= $form->field($model, 'username')->textInput() ?>

	    		</div>
	    		<div class="col-md-6">
	    			
	    			<?= $form->field($model, 'email')->textInput() ?>

	    		</div>
	    	</div>
	    	<div class="row">
	    		<div class="col-md-6">
	    			
	    			<?= $form->field($model, 'resetPassword')->passwordInput(['maxLength' => true]) ?>

	    		</div>
	    		<div class="col-md-6">
	    			
	    			<?= $form->field($model, 'confirmPassword')->passwordInput(['maxLength' => true]) ?>

	    		</div>
	    	</div>
	    	<div class="form-group">
		        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
		    </div>
    	</div>

    	<?php ActiveForm::end(); ?>

	</div>
</div>
