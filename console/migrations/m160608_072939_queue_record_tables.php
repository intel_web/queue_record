<?php

use yii\db\Schema;
use yii\db\Migration;

class m160608_072939_queue_record_tables extends Migration
{
    public function safeUp() {
        $this->createTable('earlier_appeal', [
            'id' => Schema::TYPE_PK,
            'instance' => Schema::TYPE_STRING.' NOT NULL',
        ]);

        $this->insert('earlier_appeal', [
            'instance' => 'Другая инстанция',
        ]);
        
        $this->createTable('reception_day', [
            'id' => Schema::TYPE_PK,
            'reception_date' => Schema::TYPE_DATE.' NOT NULL',
            'status' => Schema::TYPE_BOOLEAN.' DEFAULT 0',
        ]);

        $this->createTable('reception_time', [
            'id' => Schema::TYPE_PK,
            'reception_day_id' => Schema::TYPE_INTEGER.' NOT NULL',
            'rec_time' => Schema::TYPE_TIME.' NOT NULL',
            'status' => Schema::TYPE_BOOLEAN.' DEFAULT 0',
        ]);
        $this->createIndex('FK_reception_time_reception_day_id', 'reception_time', 'reception_day_id');
        $this->addForeignKey('FK_reception_time_reception_day_id', 'reception_time', 'reception_day_id', 'reception_day', 'id', 'RESTRICT', 'RESTRICT');

        $this->createTable('classifier', [
            'id' => Schema::TYPE_PK,
            'classifier_name' => Schema::TYPE_STRING.' NOT NULL',
        ]);

        $this->createTable('order', [
            'id' => Schema::TYPE_PK,
            'citizen_name' => Schema::TYPE_STRING.' NOT Null',
            'sex' => Schema::TYPE_BOOLEAN.' NOT NULL',
            'birth_date' => Schema::TYPE_DATE.' NOT NULL',
            'age' => Schema::TYPE_STRING.' NOT NULL',
            'city' => Schema::TYPE_STRING.' NOT NULL',
            'street' => Schema::TYPE_STRING.' NOT NULL',
            'house' => Schema::TYPE_STRING.' NOT NULL',
            'apartment_number' => Schema::TYPE_STRING,
            'phone' => Schema::TYPE_STRING,            
            'email' => Schema::TYPE_STRING,
            'classifier_id' => Schema::TYPE_INTEGER.' NOT NULL',
            'appeal' => Schema::TYPE_TEXT.' NOT NULL',
            'earlier_appeal_id' => Schema::TYPE_INTEGER.' NOT NULL',
            'earlier_appeal_text' => Schema::TYPE_STRING,
            'reception_time_id' => Schema::TYPE_INTEGER.' NOT NULL',
        ]);
        $this->createIndex('FK_order_earlier_appeal_id', 'order', 'earlier_appeal_id');
        $this->createIndex('FK_order_reception_time_id', 'order', 'reception_time_id');
        $this->createIndex('FK_order_classifier_id', 'order', 'classifier_id');
        $this->addForeignKey('FK_order_earlier_appeal_id', 'order', 'earlier_appeal_id', 'earlier_appeal', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('FK_order_reception_time_id', 'order', 'reception_time_id', 'reception_time', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('FK_order_classifier_id', 'order', 'classifier_id', 'classifier', 'id', 'RESTRICT', 'RESTRICT');

        $this->createTable('administrator', [
            'user_id' => Schema::TYPE_PK,
            'surname' => Schema::TYPE_STRING,
            'name' => Schema::TYPE_STRING,
            'lastname' => Schema::TYPE_STRING,
            'phone' => Schema::TYPE_STRING,
        ]);
        $this->createIndex('FK_administrator_user_id', 'administrator', 'user_id');
        $this->addForeignKey('FK_administrator_user_id', 'administrator', 'user_id', 'user', 'id', 'RESTRICT', 'RESTRICT');
    }

    public function safeDown() { 
        $this->dropTable('order');
        $this->dropTable('reception_time');
        $this->dropTable('reception_day');
        $this->dropTable('earlier_appeal');
        $this->dropTable('administrator');
        $this->dropTable('classifier');
    }
}
